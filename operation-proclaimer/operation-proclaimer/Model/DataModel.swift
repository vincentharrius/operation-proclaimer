//
//  DataModel.swift
//  operation-proclaimer
//
//  Created by Robin Kalla on 2018-11-23.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import Foundation
import UIKit
import CoreMotion


struct HighscoreEntry {
    var steps: UInt32 = 0;
    var userID: UInt32 = 0;
    var username: String!;
    var pic: UIImage?
}

// MARK: User Class
class User : NSObject, NSCoding{
    
    var name: String!
    private var picture: UIImage?
    private var goals: [Goal]!
    private var settings: Settings!
    private var stepCounter: StepCounter!
    var placementToday: Int?
    var placementTotal: Int?

    
    required init?(coder aDecoder: NSCoder) {
        if let name = aDecoder.decodeObject(forKey: "name") as? String {
            self.name = name
        }
        else{
            return nil
        }
        
        if let picture = aDecoder.decodeObject(forKey: "picture") as? UIImage {
            self.picture = picture
        }
        else{
            return nil
        }
        
        if let goals = aDecoder.decodeObject(forKey: "goals") as? [Goal]{
            self.goals = goals
        
        }
        else{
            return nil
        }
 
        if let settings = aDecoder.decodeObject(forKey: "settings") as? Settings {
            self.settings = settings
        }
        else{
            return nil
        }
        
        if let stepCounter = aDecoder.decodeObject(forKey: "stepCounter") as? StepCounter {
            self.stepCounter = stepCounter
        }
        else{
            return nil
        }
        
    }
    
    func encode(with aCoder: NSCoder) {
        
        aCoder.encode(name!, forKey: "name")
        aCoder.encode(picture!, forKey: "picture")
        aCoder.encode(goals!, forKey: "goals")
        aCoder.encode(settings!, forKey: "settings")
        aCoder.encode(stepCounter!, forKey: "stepCounter")
        
    }
    
    
    init(_ name:String = "User",_ picture:UIImage = UIImage(named: "default")!,_ settings: Settings = Settings(),_ stepCounter: StepCounter = StepCounter(),_ noOfGoals: Int = 0 ) {
        self.name = name
        self.picture = picture
        self.goals = []
        self.settings = settings
        self.stepCounter = stepCounter
        
        self.goals.append(Goal(Date(),8000,0,0))
    }
    // MARK: User Class Getters and Setters
    func set_profile_picture(_ picture: UIImage){
        self.picture = picture
    }
    
    func get_profile_picture() -> UIImage? {
        return self.picture
    }
    
    func set_name(_ name: String){
        self.name = name
    }
    
    func get_name() -> String{
        return self.name
    }
    
    func add_goal(_ goal: Int){
        let newgoal = Goal(Date(),goal,0,0)
        self.goals.append(newgoal)
    }
    
    func get_setting() -> Settings{
        return self.settings
    }
    
    func get_no_of_goals() -> Int{
        return self.goals.count
    }
    
    func get_goals() -> [Goal]{
        return self.goals
    }
    
    func get_selected_goal(_ index: Int) -> Goal{
        return self.goals[index-1]
    }
    
    func get_step_counter() -> StepCounter{
        return self.stepCounter
    }
    
    
}

// MARK: Goal Class
class Goal: NSObject, NSCoding{
    
    private let creationDate: Date
    private var goal: Int
    private var progress: Int
    private var steps: Int
    private var backgroundProgress: Int
    
    required init?(coder aDecoder: NSCoder) {
        if let creationDate = aDecoder.decodeObject(forKey: "creationDate") as? Date {
            self.creationDate = creationDate
        }
        else{
            return nil
        }
        
        self.goal = aDecoder.decodeInteger(forKey: "goal")
        self.progress = 0
        self.steps = aDecoder.decodeInteger(forKey: "steps")
        self.backgroundProgress = 0
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(creationDate, forKey: "creationDate")
        aCoder.encode(goal, forKey: "goal")
        aCoder.encode(progress, forKey: "progress")
        aCoder.encode(steps, forKey: "steps")
    }
    
    

    init(_ creationDate:Date = Date(),_ goal:Int = 10000,_ progress:Int = 0,_ steps:Int = 0, backgroundProgress: Int = 0 ) {
        self.creationDate = creationDate
        self.goal = goal
        self.progress = progress
        self.steps = steps
        self.backgroundProgress = backgroundProgress
    }
    
    // MARK: Goal Class Getters and Setters
    func get_date() -> Date{
        return self.creationDate
    }
    
    func get_goal() -> Int{
        return self.goal
    }
    
    func set_steps(_ progress: Int){
        self.steps = self.steps + progress + self.backgroundProgress
    }
    
    func set_background_progress(_ inc_prog: Int) {
        self.backgroundProgress = inc_prog
    }
    
    func set_progress(_ inc_prog: Int){
        self.progress = inc_prog
    }
    
    func get_steps() -> Int {
        return self.steps + self.progress + self.backgroundProgress
    }
    
    func get_progress() -> Int{
        return self.progress
    }
    
    func set_goal(_ goal: Int){
        self.goal = goal
    }
    
    
}

// MARK: Settings Class
class Settings: NSObject, NSCoding{
    
    required init?(coder aDecoder: NSCoder) {
        

        self.chosenTheme = aDecoder.decodeInteger(forKey: "chosenTheme") | 0
        
        
        if let themeArr = aDecoder.decodeObject(forKey: "themeArr") as? [Themes] {
            self.themeArr = themeArr
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(chosenTheme, forKey: "chosenTheme")
        aCoder.encode(themeArr, forKey: "themeArr")
    }
    
    private var chosenTheme: Int
    
    
    private var themeArr: [Themes] = [
        Themes("Pure Purple", UIColor(red: 141/255.0, green: 77/255.0, blue: 168/255.0, alpha: 1.0)),
        Themes("Rad Red", UIColor(red:0.90, green:0.29, blue:0.29, alpha:1.0)),
        Themes("Gravy Green",UIColor(red:0/255.0, green:135/255.0, blue:56/255.0, alpha:1.0)),
        Themes("Dank Dark", UIColor(red:71/255.0, green:71/255.0, blue:71/255.0, alpha:1.0))]
    
    init(chosenTheme: Int = 0) {
        self.chosenTheme = chosenTheme

    }
    
    // MARK: Settings Class Getters and Setters
    func get_chosen_theme() -> UIColor{
        return self.themeArr[self.chosenTheme].get_main_color()
    }
    func get_theme_index() -> Int{
        return self.chosenTheme
    }
    
    func set_chosen_theme(index: Int) {
        self.chosenTheme = index
    }
    
    func get_available_themes() -> [Themes] {
        return themeArr
    }

}

// MARK: Themes Class
class Themes: NSObject, NSCoding{
    
    required init?(coder aDecoder: NSCoder) {
        
        if let themeName = aDecoder.decodeObject(forKey: "themeName") as? String {
            self.themeName = themeName
        }
        else{
            return nil
        }
        if let mainColor = aDecoder.decodeObject(forKey: "mainColor") as? UIColor {
            self.mainColor = mainColor
        }
        else{
            return nil
        }

    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(themeName!, forKey: "themeName")
        aCoder.encode(mainColor!, forKey: "mainColor")
    }
    
    private let themeName: String!
    private let mainColor: UIColor!

    
    init(_ themeName: String = "Purple",_ mainColor: UIColor = UIColor(red: 141/255.0, green: 77/255.0, blue: 168/255.0, alpha: 1.0)){
        self.themeName = themeName
        self.mainColor = mainColor

    }
    // MARK: Themes Class Getters and Setters
    func get_theme_name() -> String{
        return self.themeName
    }
    
    func get_main_color() -> UIColor{
        return self.mainColor
    }
    
}

// MARK: StepCounter Class
class StepCounter: NSObject, NSCoding{
    
    
    required init?(coder aDecoder: NSCoder) {

        self.pedometer = CMPedometer()

        
        if let appShutDownDate = aDecoder.decodeObject(forKey: "appShutDownDate") as? Date {
            self.appShutDownDate = appShutDownDate
        }
        else{
            return nil
        }
        
        if let appStartedDate = aDecoder.decodeObject(forKey: "appStartedDate") as? Date {
            self.appStartedDate = appStartedDate
        }
        else{
            return nil
        }
        
        if let appLastStarted = aDecoder.decodeObject(forKey: "appLastStarted") as? Date {
            self.appLastStarted = appLastStarted
        }
        else{
            self.appLastStarted = nil
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(appShutDownDate!, forKey: "appShutDownDate")
        aCoder.encode(appStartedDate!, forKey: "appStartedDate")
        if let appLastStarted = appLastStarted{
            aCoder.encode(appLastStarted, forKey: "appLastStarted")
        }
        
    }
    
    private let pedometer:CMPedometer!
    private var appShutDownDate: Date!
    private var appStartedDate: Date!
    private var appLastStarted: Date?
    private var mutex = DispatchQueue(label: "pedoMutex")
    
    init(_ pedometer: CMPedometer = CMPedometer(),_ appShutDownDate: Date = Date(),_ appStartedDate: Date = Date()) {
        self.pedometer = pedometer
        self.appShutDownDate = appShutDownDate
        self.appStartedDate = appStartedDate
        self.appLastStarted = nil
    }
    
    // MARK: StepCounter Class Pedometer functions
    func query_pedometer(){
        if(CMPedometer.isStepCountingAvailable()){
            mutex.sync {
                appStartedDate = Date()
                
                if(appLastStarted != nil){
                    
                    if(DataHandler.instance.user!.get_step_counter().check_date()){
                        pedometer.queryPedometerData(from: appShutDownDate, to: appStartedDate, withHandler: {(data,error) in
                            if(error == nil)  {
                                let newSteps = data!.numberOfSteps.intValue
                                let noGoals = DataHandler.instance.user!.get_no_of_goals()
                                DataHandler.instance.user!.get_selected_goal(noGoals).set_background_progress(newSteps)
                            }
                        })
                    }
                }
                else{
                    appLastStarted = Date()
                }
            }
        }
    }
    
    func pedometer_start(){
        if(CMPedometer.isStepCountingAvailable()){
            mutex.sync {
                let user = DataHandler.instance.user!
                if(user.get_step_counter().check_date()){
                    appStartedDate = Date()
                    appLastStarted = appStartedDate
                    pedometer.startUpdates(from: appStartedDate) { (data, error) in
                        let newSteps = data!.numberOfSteps.intValue
                        let noGoals = DataHandler.instance.user!.get_no_of_goals()
                        DataHandler.instance.user!.get_selected_goal(noGoals).set_progress(newSteps)
                        if let con = Connection(){
                            let steps = DataHandler.instance.user!.get_selected_goal(noGoals).get_steps()
                            if let placement = con.updateSteps(stepsToday: UInt32(steps)){
                                user.placementToday = Int(placement.placementToday)
                                user.placementTotal = Int(placement.placementTotal)
                            }
                        }
                    }
                }
            }
        }
    }
    
    func pedometer_end(){
        if(CMPedometer.isStepCountingAvailable()){
            mutex.sync {
                pedometer.stopUpdates()
                let noOfGoals: Int = DataHandler.instance.user!.get_no_of_goals()
                let progress: Int = DataHandler.instance.user!.get_selected_goal(noOfGoals).get_progress()
                DataHandler.instance.user?.get_selected_goal(noOfGoals).set_steps(progress)
                DataHandler.instance.user?.get_selected_goal(noOfGoals).set_progress(0)
                appShutDownDate = Date()
            }
        }
    }
 
    func check_date() -> Bool{
        let noGoals = DataHandler.instance.user?.get_no_of_goals()
        let creationDate: Date = DataHandler.instance.user!.get_selected_goal(noGoals!).get_date()
        return Calendar.current.isDate(Date(), inSameDayAs: creationDate)
    }
    

}

// MARK: DataHandler CLASS
class DataHandler: NSObject{
    static let instance = DataHandler()
    
    var dailyHighscore: [HighscoreEntry] = []
    var totalHighscore: [HighscoreEntry] = []
    var user: User?
    let USER_STRING: String = "user"
    
    
    // MARK: Local data
    func saveUserDefaults(){
        let encodedUser = NSKeyedArchiver.archivedData(withRootObject: user as Any)
        UserDefaults.standard.set(encodedUser, forKey: USER_STRING)
    }
    
    func loadUserDefaults(){
        if let userData = UserDefaults.standard.object(forKey: USER_STRING) as? Data{
            if let user = NSKeyedUnarchiver.unarchiveObject(with:userData) as? User{
                self.user = user
            }
        }
    }
    
    // MARK: Server data
    func uploadSteps() -> Bool
    {
        if let user = self.user {
            if let con = Connection(), let _ = con.updateSteps(stepsToday: UInt32(user.get_selected_goal(user.get_no_of_goals()).get_steps()))
            {
                con.closeSocketAndStreams()
                return true
            }
        }
        return false
    }
    
    func downloadHighscores() -> Bool
    {
        var daily: [HighscoreEntry]!
        var total: [HighscoreEntry]!
        
        let user = DataHandler.instance.user!
        
        if let con = Connection(), let fetch = con.fetchHighscore(daily: true)
        {
            daily = fetch.entries
            user.placementToday = Int(fetch.userPlacement)
            con.closeSocketAndStreams()
        } else { return false }
        
        if let con = Connection(), let fetch = con.fetchHighscore(daily: false)
        {
            total = fetch.entries
            user.placementToday = Int(fetch.userPlacement)
            con.closeSocketAndStreams()
        } else { return false }
        
        dailyHighscore = daily
        totalHighscore = total
        
        return true
    }

}
