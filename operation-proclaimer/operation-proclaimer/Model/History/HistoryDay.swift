//
//  HistoryDay.swift
//  operation-proclaimer
//
//  Created by Vincent Harruis on 2018-11-15.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import Foundation

class HistoryDay {
    
    var date:   String
    var steps:     Int
    var goalSteps: Int
    
    init(date: Date, steps: Int, goalSteps: Int) {
        
        // MARK: Humanifies date string
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let dateString = formatter.string(from: date)
        
        let todayDate = formatter.string(from: Date())
        
        var yesterdayInterval = DateComponents()
        yesterdayInterval.day = -1
        let yesterdayDateRaw = Calendar.current.date(byAdding: yesterdayInterval, to: Date())!
        let yesterdayDate = formatter.string(from: yesterdayDateRaw)

        var ereyesterdayInterval = DateComponents()
        ereyesterdayInterval.day = -2
        let ereyesterdayDateRaw = Calendar.current.date(byAdding: ereyesterdayInterval, to: Date())!
        let ereyesterdayDate = formatter.string(from: ereyesterdayDateRaw)
        
        if (dateString.compare(todayDate) == .orderedSame) {
            self.date = "Today"
        }
        else if (dateString.compare(yesterdayDate) == .orderedSame) {
            self.date  = "Yesterday"
        }
        else if (dateString.compare(ereyesterdayDate) == .orderedSame) {
            self.date  = "Ereyesterday"
        }
        else {
            self.date  = dateString
        }
        
        self.steps = steps
        self.goalSteps = goalSteps
    }
}
