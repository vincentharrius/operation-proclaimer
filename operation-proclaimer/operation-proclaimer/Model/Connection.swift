//
//  Connection.swift
//  operation-proclaimer
//
//  Created by Vincent Harruis on 2018-11-28.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import Foundation
import UIKit


struct PictureData
{
    var format: UInt8;
    var size: UInt32;
    var data: UnsafePointer<UInt8>;
}

class Connection {
    
    static var lastConnectOrRequestFailed = false
    
    var socketHandle: Int32!
    
    var messages: NSMutableArray = []
    
    var readStream: CFReadStream!
    var writeStream: CFWriteStream!
    
    enum RequestID: UInt8
    {
        case fetchHighscore = 1
        case updateProfile = 2
        case updateSteps = 3
    }
    
    
    init?() {
        Connection.lastConnectOrRequestFailed = true

        let ip = "18.222.195.36"
        let port = 7003
        
        let in_addr = inet_addr(ip/*"212.25.141.11"*/)
        if(in_addr == INADDR_NONE) {
            return nil
        }
        
        var sin = sockaddr_in()
        sin.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        sin.sin_family = sa_family_t(AF_INET)
        sin.sin_port = UInt16(port).bigEndian
        sin.sin_addr.s_addr = in_addr
        
        // MARK: Create socket
        
        socketHandle = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)
        
        var copy_of_sin = sin
        let cn = withUnsafeMutablePointer(to: &copy_of_sin)
        {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1)
            {
                connect(socketHandle, UnsafePointer($0), socklen_t(MemoryLayout<sockaddr_in>.size(ofValue: sin)))
            }
        }
        if(cn != 0) {
            return nil
        }
        
        
        //MARK Create streams
        
        var readStreamPtr: Unmanaged<CFReadStream>?
        var writeStreamPtr: Unmanaged<CFWriteStream>?
        
        CFStreamCreatePairWithSocket(kCFAllocatorDefault, socketHandle, &readStreamPtr, &writeStreamPtr)
        
        readStream = readStreamPtr!.takeRetainedValue()
        if(!CFReadStreamOpen(readStream)){
            return nil
        }
        
        writeStream = writeStreamPtr!.takeRetainedValue()
        if(!CFWriteStreamOpen(writeStream)){
            return nil
        }
        
        Connection.lastConnectOrRequestFailed = false
    }
    
    // MARK: Read procedures
    
    var int32Buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: MemoryLayout<Int32>.size)
    func readInt32() -> Int32?
    {
        if(CFReadStreamRead(readStream, int32Buffer, MemoryLayout<Int32>.size) != MemoryLayout<Int32>.size) { return nil }
        let int32_ptr = UnsafeRawPointer(int32Buffer).bindMemory(to: Int32.self, capacity: 1)
        let result = int32_ptr.pointee
        return result
    }
    
    var uint32Buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: MemoryLayout<UInt32>.size)
    func readUInt32() -> UInt32?
    {
        if(CFReadStreamRead(readStream, uint32Buffer, MemoryLayout<UInt32>.size) != MemoryLayout<UInt32>.size) { return nil }
        let uint32_ptr = UnsafeRawPointer(uint32Buffer).bindMemory(to: UInt32.self, capacity: 1)
        return uint32_ptr.pointee
    }
    
    var uint8Buffer = UnsafeMutablePointer<UInt8>.allocate(capacity: MemoryLayout<UInt8>.size);
    func readUInt8() -> UInt8?
    {
        if(CFReadStreamRead(readStream, uint8Buffer, MemoryLayout<UInt8>.size) != MemoryLayout<UInt8>.size) { return nil }
        return uint8Buffer.pointee
    }
    
    
    
    func readBytes(length: Int) -> UnsafeMutablePointer<UInt8>?
    {
        let uint8_ptr = UnsafeMutablePointer<UInt8>.allocate(capacity: length)
        
        var readBytes = 0;
        while(true)
        {
            let read = CFReadStreamRead(readStream, uint8_ptr.advanced(by: readBytes), length - readBytes);
            if(read <= 0) { break }
            readBytes += read;
            if(readBytes >= length) { break }
        }
        if(readBytes != length)
        {
            uint8_ptr.deallocate()
            return nil
        }
        
        return uint8_ptr
    }
    
    func readString() -> String?{
        
        if let length = readUInt32()
        {
            if length == 0 { return "" }
            if let bytes = readBytes(length: Int(length))
            {
                return String(bytes: Array(UnsafeMutableBufferPointer(start: bytes, count: Int(length))), encoding: .utf8)
            }
        }
        
        return nil
        
    }
    
    
    struct ResponseHeader
    {
        var error: UInt8?
    }
    
    func readResponseHeader() -> ResponseHeader?
    {
        if let responseID = readUInt8()
        {
            if responseID != 1
            {
                if let error = readUInt8()
                {
                    return ResponseHeader(error: error)
                } else { return nil }
            }
            return ResponseHeader(error: nil)
        }
        return nil;
    }
    

    // MARK: Write procedures
    
    func writeUInt8(_ integer: UInt8) -> Bool{
        
        uint8Buffer.pointee = integer;
        return CFWriteStreamWrite(writeStream, uint8Buffer, MemoryLayout<UInt8>.size) == MemoryLayout<UInt8>.size
    }
    
    func writeUInt32(_ integer: UInt32) -> Bool{
        
        let uint32Ptr = UnsafeMutablePointer<UInt32>.allocate(capacity: 1)
        uint32Ptr.pointee = integer
        
        uint32Buffer.assign(from: UnsafeRawPointer(uint32Ptr).bindMemory(to: UInt8.self, capacity: 4), count: 4)
        return CFWriteStreamWrite(writeStream, uint32Buffer, MemoryLayout<UInt32>.size) == MemoryLayout<UInt32>.size
    }
    
    func writeString(_ string: String) -> Bool{
        
        let length = UInt32(string.utf8.count)
        let str_data = string.utf8
        var str_data_it = str_data.makeIterator()
 
        if (!writeUInt32(length)) {
            return false
        }
        
        while let byte = str_data_it.next(){
            if(!writeUInt8(byte)) {
                return false
            }
        }
        return true
    }
    
    func writeBytes(_ ptr: UnsafePointer<UInt8>, _ n: CFIndex) -> Bool
    {
        if(n == 0) { return true }
        return CFWriteStreamWrite(writeStream, ptr, n) == n
    }
    
    func writePicture(_ pic: PictureData) -> Bool {
        if(!writeUInt8(pic.format)) { return false }
        if(!writeUInt32(pic.size)) { return false }
        if(!writeBytes(pic.data, CFIndex(pic.size))) { return false }
        return true
    }
    
    func writeRequestHeader(reqID: RequestID, username: String) -> Bool
    {
        if(!writeUInt8(reqID.rawValue)) { return false }
        if(!writeString(username)){ return false }
        return true
    }
    
    
    
    // MARK: Requests
    
    func fetchHighscore(daily: Bool) -> (entries: [HighscoreEntry], userPlacement: Int32)?{
        Connection.lastConnectOrRequestFailed = true
        
        if let user = DataHandler.instance.user
        {
            // MARK: temp
            let username = user.name!
            
            if(!writeRequestHeader(reqID: .fetchHighscore, username: username)) { return nil }
            
            let total: UInt8 = (daily) ? 0 : 1
            if(!writeUInt8(total)) { return nil }
            
            if let responseHeader = readResponseHeader() {
                if(responseHeader.error != nil) { return nil }
            } else { return nil }
            
            var entries: [HighscoreEntry] = []
            var pictureData: [PictureData] = []
            
            if let numRows = readUInt8() {
                for _ in 0..<numRows {
                    if let numSteps = readUInt32() {
                        var entry = HighscoreEntry()
                        entry.steps = numSteps
                        entries.append(entry)
                    }
                    else { return nil }
                }
                
                for row in 0..<Int(numRows) {
                    if let userID = readUInt32(){
                        entries[row].userID = userID;
                    } else { return nil }
                    
                    if let username = readString(){
                        entries[row].username = username;
                    } else { return nil }
                    
                    if(row < 3)
                    {
                        var read_picture = false;
                        if let format = readUInt8(){
                            if let size = readUInt32(){
                                if size == 0 {
                                    pictureData.append(PictureData(format: format, size: size, data: UnsafeMutablePointer<UInt8>.allocate(capacity: 0)));
                                    read_picture = true
                                }
                                else if let data = readBytes(length: Int(size))
                                {
                                    pictureData.append(PictureData(format: format, size: size, data: data));
                                    read_picture = true;
                                }
                            }
                        }
                        
                        if(!read_picture){
                            return nil;
                        }
                    }
                    
                    // profile pic
                }
                
                for row in 0..<min(3, entries.count) {
                    entries[row].pic = UIImage(data: Data(buffer: UnsafeBufferPointer(start: pictureData[row].data, count: Int(pictureData[row].size))))
                }
            }
            
            if let userPlacement = readInt32()
            {
                Connection.lastConnectOrRequestFailed = false
                return (entries: entries, userPlacement: userPlacement)
            }
        }
        
        return nil
    }
    
    func updateProfile(name: String, pic: PictureData) -> Bool
    {
        Connection.lastConnectOrRequestFailed = true
        
        if let user = DataHandler.instance.user
        {
            // MARK: temp
            let username = user.name!
            
            if(!writeRequestHeader(reqID: .updateProfile, username: username)) { return false }
            if(!writeString(name)) { return false }
            if(!writePicture(pic)) { return false }
            
            if let responseHeader = readResponseHeader() {
                if(responseHeader.error != nil) {
                    return false }
            } else { return false }
            
            Connection.lastConnectOrRequestFailed = false
        }
        return true
    }
    
    
    func updateSteps(stepsToday: UInt32) -> (placementToday: Int32, placementTotal: Int32)?
    {
        Connection.lastConnectOrRequestFailed = true
        if let user = DataHandler.instance.user
        {
            // MARK: temp
            let username = user.name!
            
            if(!writeRequestHeader(reqID: .updateSteps, username: username)) { return nil }
            if(!writeUInt32(stepsToday)) { return nil }
            
            if let responseHeader = readResponseHeader() {
                if(responseHeader.error != nil) { return nil }
            } else { return nil }
            
            if let placementToday = readInt32() {
                if let placementTotal = readInt32()
                {
                    Connection.lastConnectOrRequestFailed = false
                    return (placementToday: placementToday, placementTotal: placementToday)
                }
            }
        }
        
        return nil
    }
    
    
    func closeSocketAndStreams()
    {
        CFReadStreamClose(readStream)
        CFWriteStreamClose(writeStream)
        close(socketHandle)
    }
}
