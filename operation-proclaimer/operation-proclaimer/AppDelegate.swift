//
//  AppDelegate.swift
//  operation-proclaimer
//
//  Created by Edvin Holm on 2018-11-02.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import UIKit
import GoogleSignIn
import UserNotifications


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?

    
    //@MARK: PUSH NOTIFICATIONS
    
    //The return (apple id) worked
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //let token = deviceToken.map{String(format: "%02.2hhx", $0)}.joined()
        //print(token)
    }
    
    // The return (apple token) did not work
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
    }
    
    //WILL SHOW THE NOTIFICATION WHEN APP IS ACTIVE
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        
    }

    //HANDLE THE PUSH NOTOFICATION
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        

        completionHandler()
        center.removeAllDeliveredNotifications()
    }
    
    // Parser
    private func parseRemoteNotification(notification:[String:AnyObject]) -> String? {
        if let aps = notification["aps"] as? [String:AnyObject] {
            let alert = aps["alert"] as? String
            return alert
        }
        return nil
    }


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        DataHandler.instance.loadUserDefaults()
        
        UNUserNotificationCenter.current().delegate = self
        
        
        UNUserNotificationCenter.current().requestAuthorization(options: ([.alert, .badge, .sound])) { (granted, error) in
        }
        UIApplication.shared.registerForRemoteNotifications()
        
        // MARK: LOCAL NOTIFICATION
        let content = UNMutableNotificationContent()
        content.body = "The time is 13:37, Time to get moving"
        content.sound = UNNotificationSound.default()
        
        var triggertime = DateComponents()
        triggertime.hour = 13
        triggertime.minute = 37
        triggertime.second = 0
        let trigger = UNCalendarNotificationTrigger(dateMatching: triggertime, repeats: true)
        
        let request = UNNotificationRequest(identifier: "1337trigger", content: content, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request,withCompletionHandler: nil)
        
        // MARK: GoogleSignIn Init
        GIDSignIn.sharedInstance().clientID = "127542528393-6gku5kdkgjb6vfj25dok3srcb3gh4g7i.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().delegate = self
        
        if(DataHandler.instance.user != nil) {
           add_default_goal()
        }
        
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        if let user = DataHandler.instance.user
        {
            user.get_step_counter().pedometer_end()
        }
        DataHandler.instance.saveUserDefaults()
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        DataHandler.instance.user?.get_step_counter().query_pedometer()
        //query_pedometer needs to happend before .pedometer_start
        DataHandler.instance.user?.get_step_counter().pedometer_start()
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                                 sourceApplication: options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: options[UIApplicationOpenURLOptionsKey.annotation])
    }
    
    // MARK: Actions when user is signed in through Google
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
        if let _ = error {
        } else {
            
            let fullName = user.profile.name
            
            DataHandler.instance.user = User();
            
            add_default_goal()
            
            DataHandler.instance.user?.get_step_counter().query_pedometer()
            DataHandler.instance.user?.get_step_counter().pedometer_start()
            
            if user.profile.hasImage {
                
                if let url = user.profile.imageURL(withDimension: 400) {
                    URLSession.shared.dataTask(with: url, completionHandler: { data, response, error in
                        guard let data = data, error == nil else { return }
                        
                        DispatchQueue.main.async() {
                            var profilePicture = UIImage(data: data)
                            DataHandler.instance.user!.set_profile_picture(profilePicture!)
                            
                            // send data to server
                            let format = UInt8(2)
                            let size = UInt32(data.count)
                            
                            let uint8Ptr = (data as NSData).bytes.bindMemory(to: UInt8.self, capacity: Int(size))
                            let pictureData = PictureData(format: format, size: size, data: uint8Ptr)
                            
                            profilePicture = UIImage(data: Data(buffer: UnsafeBufferPointer(start: pictureData.data, count: Int(pictureData.size))))
                            DataHandler.instance.user!.set_profile_picture(profilePicture!)
                            
                            if let connection = Connection() {
                                if connection.updateProfile(name: fullName ?? "username", pic: pictureData) {
                                } else {
                                }
                                connection.closeSocketAndStreams()
                            }
                            
                        }
                    }).resume()
                }
                
                
            }
            
            // puts user variables in DataHandler
            DataHandler.instance.user!.name = fullName ?? "Mr Unknown"
            
        }
        
        if let color = DataHandler.instance.user?.get_setting().get_chosen_theme()
        {
            mainviewController.Liquid.set_Color(color)
        }
        mainviewController.dismiss(animated: true)
    }

    func add_default_goal() {
        
        let noOfGoals = DataHandler.instance.user?.get_no_of_goals()
        
        let latestGoal = DataHandler.instance.user?.get_selected_goal(noOfGoals!).get_goal()
        
        if(DataHandler.instance.user?.get_step_counter().check_date() == true){
            DataHandler.instance.user?.get_selected_goal(noOfGoals!).set_goal(latestGoal!)
        }
        else if (latestGoal == nil){
            DataHandler.instance.user!.add_goal(8000)
        }
        else {
            DataHandler.instance.user!.add_goal(latestGoal!)
        }
    }
    
    
}
