//
//  HighscoreCell.swift
//  operation-proclaimer
//
//  Created by Robin Kalla on 2018-11-15.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import UIKit

class HighscoreCell: UITableViewCell {

    @IBOutlet weak var cellPos: UILabel!
    @IBOutlet weak var cellName: UILabel!
    @IBOutlet weak var cellScore: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
