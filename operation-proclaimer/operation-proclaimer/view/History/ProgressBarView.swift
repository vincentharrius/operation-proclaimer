//
//  ProgressBarView.swift
//  
//
//  Created by Vincent Harruis on 2018-11-15.
//

import UIKit

class ProgressBarView: UIView {

    private var _innerProgress: CGFloat = 0.0
    
    // MARK: progress will represent the length of the progressbar
    var progress:CGFloat {
        set (newProgress) {
            if newProgress > 1.0 {
                _innerProgress = 1.0
            }
            else if newProgress < 0.0 {
                _innerProgress = 0.0
            }
            else {
                _innerProgress = newProgress
            }
        }
        get {
            return _innerProgress * bounds.width
        }
    }
    
    override func draw(_ rect: CGRect) {
        ProgressBarDraw.drawProgressBar(frame: bounds, progress: progress)
    }

}
