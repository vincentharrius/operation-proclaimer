//
//  HistoryTableCell.swift
//  operation-proclaimer
//
//  Created by Vincent Harruis on 2018-11-15.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import UIKit

class HistoryTableCell: UITableViewCell {

    @IBOutlet weak var DateLabel: UILabel!
    @IBOutlet weak var ProgressBar: ProgressBarView!
    @IBOutlet weak var StepsLabel: UILabel!
    
    // MARK: Displays Day data in the cell
    func setupDayCell(day: HistoryDay) {
        DateLabel.text = day.date
        
        ProgressBar.progress = CGFloat(day.steps) / CGFloat(day.goalSteps)
        ProgressBar.setNeedsDisplay()
        
        StepsLabel.text = String(day.steps) + " / " + String(day.goalSteps)
    }
    
}
