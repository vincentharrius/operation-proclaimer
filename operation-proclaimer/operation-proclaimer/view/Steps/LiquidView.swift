

import Foundation
import UIKit


class LiquidView: UIView
{
    
    var fillLevel = CGFloat(0.0);
    var liquid : UIView!
    var waveView: UIView!
    var wave1 : UIImageView!
    var wave2 : UIImageView!
    var backWave1 : UIImageView!
    var backWave2 : UIImageView!
    var origianalY: CGFloat!
    var percent: UILabel!
    var total: UILabel!
    var textY: CGFloat!
    var percentNewValue: CGFloat!
    var totalNewValue: CGFloat!
    var waveViewNewValue: CGFloat!
    var liquidNewValue: CGFloat!
    var themeColor: UIColor!
    
    
    var steps: Int!
    var goal: Int!

    // MARK: Initial Animations
    func animateAll(){
        
        
        animateHeight()
        animateSlideTemplate(wave1,"ani1",self.frame.width*0.5,self.frame.width*1.5,4)
        animateSlideTemplate(wave2,"ani2",-self.frame.width*0.5,self.frame.width*0.5,4)
        
        animateSlideTemplate(backWave1,"backAni1",self.frame.width*0.5,self.frame.width*1.5,6)
        animateSlideTemplate(backWave2,"backAni2",-self.frame.width*0.5,self.frame.width*0.5,6)
        
    }
    // MARK: Create All UIView
    func createAll(){
        
        liquid = UIView()
        liquid.layer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        liquid.frame = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: (self.frame.height) + 100)
        self.addSubview(liquid);
        liquid.backgroundColor = themeColor
        
        waveView = UIView()
        waveView.frame = CGRect(x: self.frame.width/2, y: self.frame.height, width: self.frame.width, height: 25)
        waveView.layer.anchorPoint = CGPoint(x: 1.0,y: 1.0)
        self.addSubview(waveView)
        
        let waveImage = UIImage(named: "wave.png")!
        wave1 = UIImageView(image: waveImage)
        wave2 = UIImageView(image: waveImage)
        
        let waveImageAlph = UIImage(named: "wave.png")!.alpha(0.3)
        backWave1 = UIImageView(image: waveImageAlph)
        backWave2 = UIImageView(image: waveImageAlph)
        
        self.waveView.addSubview(wave1)
        self.waveView.addSubview(wave2)
        self.waveView.addSubview(backWave1)
        self.waveView.addSubview(backWave2)
    
        wave1.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: (20))
        wave1.image = wave1.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        wave1.tintColor = themeColor
        
        wave2.frame = CGRect(x: -self.frame.width, y: 0, width: self.frame.width, height: 20)
        wave2.image = wave2.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        wave2.tintColor = themeColor
        
        backWave1.frame = CGRect(x: 0, y: -5, width: self.frame.width, height: 25)
        backWave1.image = backWave1.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        backWave1.tintColor = themeColor
        
        backWave2.frame = CGRect(x: -self.frame.width, y: -5, width: self.frame.width, height: 25)
        backWave2.image = backWave2.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        backWave2.tintColor = themeColor
        
        waveView.frame.size.height = wave1.frame.height;
        
        percent = UILabel(frame: CGRect(x:self.frame.width/2,y: self.frame.height ,width: 250,height: 50))
        percent.textAlignment = .center
        percent.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        percent.center.x = self.center.x
        percent.font = UIFont(name: "Avenir", size: 50.0)
        
        total = UILabel(frame: CGRect(x:self.frame.width/2,y: self.frame.height ,width: 250,height: 50))
        total.textAlignment = .center
        total.layer.anchorPoint = CGPoint(x: 0.5, y: 0.5)
        total.center.x = self.center.x
        total.font = UIFont(name: "Avenir", size: 25.0)

        
        self.addSubview(percent)
        self.addSubview(total)
        self.bringSubview(toFront: percent)
        self.bringSubview(toFront: total)
        
    }
    // MARK: sets new colors after a new theme has been set
    func set_Color(_ color: UIColor){
     
        
        wave1.tintColor = color
        wave2.tintColor = color
        backWave1.tintColor = color
        backWave2.tintColor = color
        liquid.backgroundColor = color
        percent.textColor = color
        total.textColor = color
        
    }
    //MARK: Animates Text
    func showText(){
        total.textColor = themeColor
        percent.textColor = themeColor
        
        let text = (self.fillLevel * 100)
        let stringFromDouble = NSString(format: "%.f", text)
        let resultString = stringFromDouble as String
        percent.text = resultString + "%"
        total.text = String(steps ?? 0) + "/" + String(goal ?? 10000)
        
        let percentUp = CABasicAnimation(keyPath: "position.y")
        percentUp.fromValue = percent.layer.position.y
        percentUp.duration = 1
        percentUp.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        
        let totalUp = CABasicAnimation(keyPath: "position.y")
        totalUp.fromValue = total.layer.position.y
        totalUp.duration = 1
        totalUp.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        
        if( fillLevel > 0.8 && fillLevel < 1.0){
            total.textColor = .white
            percent.textColor = .white
            percentNewValue = (self.frame.height * (1.0 - self.fillLevel)) + 75
            totalNewValue = (self.frame.height * (1.0 - self.fillLevel) + 75) + 50
            percentUp.toValue = percentNewValue
            totalUp.toValue = totalNewValue
            
            
            
        } else if (fillLevel < 0.1){
            
            
            percentNewValue = self.frame.height * (1.0 - self.fillLevel) - 125
            totalNewValue = (self.frame.height * (1.0 - self.fillLevel) - 125) + 50
            percentUp.toValue = percentNewValue
            totalUp.toValue = totalNewValue
            
            
        } else if(fillLevel >= 1.0)  {
            total.textColor = .white
            percent.textColor = .white
            percentNewValue = ((self.frame.height/2) + 75 )
            totalNewValue = ((self.frame.height/2) + 75 ) + 50
            percentUp.toValue = percentNewValue
            totalUp.toValue = totalNewValue
            
        } else {
            total.textColor = .white
            percent.textColor = themeColor
            percentNewValue = self.frame.height * (1.0 - self.fillLevel) - 75
            totalNewValue = (self.frame.height * (1.0 - self.fillLevel) - 75) + 100
            percentUp.toValue = percentNewValue
            totalUp.toValue = totalNewValue
        }
        
        percent.layer.add(percentUp, forKey: "slideAnimation")
        percent.layer.position.y = percentNewValue
        total.layer.add(totalUp, forKey: "slideAnimation")
        total.layer.position.y = totalNewValue
        
        
    }
    
    func hideText(){
        
        let percentDown = CABasicAnimation(keyPath: "position.y")
        percentDown.fromValue = percent.layer.position.y
        percentDown.toValue = self.frame.height + 75
        percentDown.duration = 1
        percentDown.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        percent.layer.add(percentDown, forKey: "slideAnimation")
        percent.layer.position.y = self.frame.height + 75
        
        let totalUp = CABasicAnimation(keyPath: "position.y")
        totalUp.fromValue = total.layer.position.y
        totalUp.toValue = self.frame.height + 75
        totalUp.duration = 1
        totalUp.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        total.layer.add(totalUp, forKey: "slideAnimation")
        total.layer.position.y = self.frame.height + 75
    }
    // MARK: Animates Heigth
    func animateHeight(){
        
        origianalY = liquid.frame.origin.y
        let liquidUp = CABasicAnimation(keyPath: "position.y")
        liquidUp.fromValue = origianalY
        liquidUp.duration = 1
        liquidUp.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        let waveUp = CABasicAnimation(keyPath: "position.y")
        waveUp.fromValue = origianalY
        waveUp.duration = 1
        waveUp.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        
        if(fillLevel >= 1.0){
            liquidNewValue = 0
            waveViewNewValue = 0
            liquidUp.toValue = liquidNewValue
            waveUp.toValue = waveViewNewValue
        } else{
            liquidNewValue = (1.0 - fillLevel) * frame.height - waveView.frame.height/2.0
            waveViewNewValue = (1.0 - fillLevel) * frame.height - waveView.frame.height/2.0
            liquidUp.toValue = liquidNewValue
            waveUp.toValue = waveViewNewValue
        }
        
        waveView.layer.add(waveUp, forKey: "slideAnimation")
        liquid.layer.add(liquidUp, forKey: "slideAnimation")
        
        liquid.layer.position.y = waveViewNewValue
        waveView.layer.position.y = liquidNewValue
        
    }
    
    // MARK: Animate Sliding
    func animateSlideTemplate( _ img: UIImageView , _ name: String,_ from : CGFloat, _ to:CGFloat , _ dur:CFTimeInterval){
        
        let name = CABasicAnimation(keyPath: "position.x")
        name.fromValue = from
        name.toValue = to
        name.duration = dur
        name.repeatCount = .infinity
        img.layer.add(name, forKey: "slideAnimation")
        
    }
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return false
    }
    
}

extension UIImage {
    
    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}

