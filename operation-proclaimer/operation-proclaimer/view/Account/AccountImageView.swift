//
//  AccountImageView.swift
//  operation-proclaimer
//
//  Created by Josef Wakman on 2018-11-26.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import Foundation
import UIKit

var profileViewController: ProfileViewController! = nil

class ProfileViewController: UIViewController{
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    
    override func viewDidLoad() {
        profileViewController = self
        image.image = DataHandler.instance.user?.get_profile_picture()
        name.text = DataHandler.instance.user!.name
    }
}
