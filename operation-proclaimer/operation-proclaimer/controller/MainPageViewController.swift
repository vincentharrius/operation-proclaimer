
import Foundation
import UIKit


class MainPageViewController : UIPageViewController
{
	var stepsPage: UIViewController!
	var historyPage: UIViewController!
	var highscorePage: UIViewController!
	var accountPage: UIViewController!
    
    var buttons: [UIButton] = []
    var pages: [UIViewController] = []
    
    var currentPage: Int = 0;
    var currentButton: UIButton = UIButton();
    
	func setup()
	{
        // MARK: view setup
		let storyboard = UIStoryboard(name: "Main", bundle: nil)
		
		self.pages.append(storyboard.instantiateViewController(withIdentifier: "steps_page"));
		self.pages.append(storyboard.instantiateViewController(withIdentifier: "history_page"));
		self.pages.append(storyboard.instantiateViewController(withIdentifier: "highscore_page"));
		self.pages.append(storyboard.instantiateViewController(withIdentifier: "account_page"));
		
		
		setViewControllers([self.pages[0]], direction: .forward, animated: true, completion: nil)
	}
    
    func setPage(_ page: Int)
    {
     
        // MARK: page swap
        let dir: UIPageViewControllerNavigationDirection = (page > currentPage) ? .forward : .reverse
        
        setViewControllers([self.pages[page]], direction: dir, animated: true);
        
        self.currentPage = page
    }
    
	
	override init(transitionStyle style: UIPageViewControllerTransitionStyle, navigationOrientation: UIPageViewControllerNavigationOrientation, options: [String : Any]? = nil) {
		super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
		
		setup()
	}
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		
		setup()
	}
	
}
