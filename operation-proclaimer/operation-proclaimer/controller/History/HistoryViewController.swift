

import Foundation
import UIKit

class HistoryViewController: UIViewController
{
    
    @IBOutlet weak var tableView: UITableView!
    
    var days: [HistoryDay] = []
    
    // MARK: Loads the table for the first time
    override func viewDidLoad() {
        super.viewDidLoad()
        
        days = get_days()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        self.tableView.contentInset.bottom = 25
    }
    
    // MARK: Reloads the tabledata
    override func viewDidAppear(_ animated: Bool) {
        days = get_days()
        
        self.tableView.reloadData()
    }
    
    // MARK: Gets all the Day data for the cells
    func get_days() -> [HistoryDay] {
        
        let user = DataHandler.instance.user
        
        var goals: [Goal] = user!.get_goals()
        
        var days: [HistoryDay] = []
        
        for goal in goals {
            days.append(HistoryDay(date: goal.get_date(), steps: goal.get_steps(), goalSteps: goal.get_goal()))
        }
        return days
    }
}

extension HistoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return days.count
    }
    
    // MARK: Setup the cells with Day data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let day = days[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryDayCell") as! HistoryTableCell
        
        cell.setupDayCell(day: day)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
