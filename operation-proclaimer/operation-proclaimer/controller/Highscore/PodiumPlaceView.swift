//
//  PodiumPlaceView.swift
//  operation-proclaimer
//
//  Created by Edvin Holm on 2018-12-05.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import Foundation
import UIKit

class PodiumPlaceView: UIView
{
    static let bigFont = UIFont(name: "Avenir", size: 20)
    static let defaultFont = UIFont(name: "Avenir", size: 18)
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var score: UILabel!
    
    var isMe = false
    var themeColor: UIColor = UIColor.gray
    var medalColor: UIColor = UIColor.black
    
    override func awakeFromNib() {
        image.layer.borderWidth = 5
    }
    
    
    func updateColors()
    {
        let mColor = (isMe) ? themeColor : medalColor
        
        image.layer.borderColor = mColor.cgColor
        name.backgroundColor = mColor
        score.textColor = mColor
        
    }
    
    func setIsMe(_ isMe: Bool)
    {
        name.font = (isMe) ? PodiumPlaceView.bigFont : PodiumPlaceView.defaultFont
        image.layer.borderWidth = (isMe) ? 7 : 5
        image.layer.shadowOpacity = 0.2
        image.layer.shadowOffset = CGSize(width: 0, height: 3)
        
        self.isMe = isMe
        
        updateColors()
    }
    
    func setColor(color: UIColor, medalColor: UIColor? = nil)
    {
        if let medalColor = medalColor
        {
            self.medalColor = medalColor
        }
        
        self.themeColor = color
        
        updateColors()
    }
}
