	
import Foundation
import UIKit

var highscoreViewController: HighscoreViewController! = nil

class HighscoreViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    var themeColor: UIColor!
    
    @IBOutlet weak var podiumView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var periodSelector: UISegmentedControl!
    
    @IBOutlet weak var firstPlace: PodiumPlaceView!
    @IBOutlet weak var secondPlace: PodiumPlaceView!
    @IBOutlet weak var thirdPlace: PodiumPlaceView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var displayedHighscore = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        highscoreViewController = self
        
        themeColor = DataHandler.instance.user?.get_setting().get_chosen_theme()
        setColor(themeColor)
        
        // MARK Podium
        firstPlace.setColor(color: themeColor, medalColor: UIColor.medalColors.gold)
        secondPlace.setColor(color: themeColor, medalColor: UIColor.medalColors.silver)
        thirdPlace.setColor(color: themeColor, medalColor: UIColor.medalColors.bronze)
        
        // MARK TableView
        //	tableView.isScrollEnabled = false
        tableView.allowsSelection = false
    }

    func numberOfSections(in tableView: UITableView) -> Int{
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let user = DataHandler.instance.user!
        let hs = DataHandler.instance.dailyHighscore
        var numRows = max(0, hs.count - 3)
        
        if let userPlacement = (displayedHighscore == 1) ? user.placementTotal : user.placementToday
        {
            if userPlacement >= hs.count || userPlacement == -1
            {
                numRows += 1
            }
        }
        
        return numRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var hs = (displayedHighscore == 1) ? DataHandler.instance.totalHighscore : DataHandler.instance.dailyHighscore
        
        let row = indexPath.row + 3
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "HighscoreCell", for: indexPath) as? HighscoreCell {
            
            let user = DataHandler.instance.user!
            
            var username: String!
            var score: Int!
            var placement: Int!
            
            if row >= hs.count
            {
                username = user.name
                score = user.get_selected_goal(user.get_no_of_goals()).get_steps()
                placement = (displayedHighscore == 1) ? user.placementTotal : user.placementToday
            }
            else
            {
                username = hs[row].username
                score = Int(hs[row].steps)
                placement = row + 1
            }
            
            cell.cellPos.text = (placement == -1) ? "(?)" : (String(placement) + ".")
            cell.cellName.text = String(username)
            cell.cellScore.text = String(score)
            
            if username == user.name
            {
                cell.backgroundColor = themeColor
                cell.cellPos.textColor = UIColor.white
                cell.cellName.textColor = UIColor.white
                cell.cellScore.textColor = UIColor.white
            }
            else
            {
                cell.backgroundColor = (row % 2 == 0) ? UIColor(displayP3Red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0) : UIColor(displayP3Red: 0.95, green: 0.95, blue: 0.95, alpha: 1.0)
                cell.cellPos.textColor = UIColor.darkText
                cell.cellName.textColor = UIColor.darkText
                cell.cellScore.textColor = UIColor.darkText
            }
            return cell
        }
        else {
            return UITableViewCell()
        }
    }
    
    func setColor(_ theme: UIColor){
        
        themeColor = theme
        
        firstPlace.setColor(color: themeColor)
        secondPlace.setColor(color: themeColor)
        thirdPlace.setColor(color: themeColor)
        
        periodSelector.tintColor = themeColor
    }
    
    func reloadData()
    {
        tableView.reloadData()
        
        var hs = (displayedHighscore == 1) ? DataHandler.instance.totalHighscore : DataHandler.instance.dailyHighscore
        var user = DataHandler.instance.user!
        
        func update(_ v: PodiumPlaceView, hs: [HighscoreEntry], place: Int)
        {
            let user = DataHandler.instance.user!
            
            if hs.count >= place+1
            {
                v.image.image = hs[place].pic
                v.name.text = "\(place+1). " + String(hs[place].username)
                v.score.text = String(hs[place].steps)
                v.setIsMe(hs[place].username == user.name)
            }
            else if ((displayedHighscore == 1) ? user.placementToday : user.placementTotal) == place
            {
                v.image.image = user.get_profile_picture()
                v.name.text = "\(place+1). " + user.name
                v.score.text = String(user.get_selected_goal(user.get_no_of_goals()).get_steps())
                v.setIsMe(true)
            }
            else
            {
                v.image.image = nil
                v.name.text = nil
                v.score.text = nil
                v.setIsMe(false)
                
            }
            self.activityIndicator.isHidden = true
        }
        
        update(firstPlace, hs: hs, place: 0)
        update(secondPlace, hs: hs, place: 1)
        update(thirdPlace, hs: hs, place: 2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.activityIndicator.isHidden = false
        getHighScoreData()
    }
    
    func getHighScoreData() {
        DispatchQueue.global().async {
            _ = DataHandler.instance.uploadSteps()
            _ = DataHandler.instance.downloadHighscores()
            DispatchQueue.main.async {
                self.reloadData()
            }
            
        }
    }
    
    @IBAction func periodSelectorValueChanged(_ sender: Any) {
        displayedHighscore = periodSelector.selectedSegmentIndex
        reloadData()
    }
    
}


extension UIColor{
    struct medalColors{
        static let gold = UIColor(red:0.83, green:0.69, blue:0.22, alpha:1.0)
        static let silver = UIColor(red:0.75, green:0.75, blue:0.75, alpha:1.0)
        static let bronze = UIColor(red:0.80, green:0.50, blue:0.20, alpha:1.0)
        }
    }

struct myUser {
    var image_name: String
    var name : String
    var position: Int
    var score: Int
    
    init(_ name: String,_ position: Int,_ score: Int,_ image_name: String = "default"){
        self.name = name
        self.position = position
        self.score = score
        self.image_name = image_name
    }
}

class roundedCorners: UIImageView {
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let radius: CGFloat = self.bounds.size.width / 2.0
        
        self.layer.cornerRadius = radius
        self.clipsToBounds = true
    }
}
