import Foundation
import UIKit
import GoogleSignIn

var accountViewController: AccountViewController! = nil

class AccountViewController: UITableViewController, GIDSignInUIDelegate, UITextFieldDelegate
{
    
    @IBOutlet var accountTableview: UITableView!
    var goalAlert : UIAlertController!
    
    var themeAlert : UIAlertController!
    var theme : UIColor!

    @IBOutlet weak var goalButton: UIButton!
    @IBOutlet weak var themePickerButton: UIButton!
    
 
    @IBAction func themePickerButtonPressed(_ sender: Any) {
        self.present(themeAlert, animated: true, completion: nil)
    }
    
    @IBAction func goalButtonPressed(_ sender: Any) {
        self.present(goalAlert, animated: true, completion: nil)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        profileViewController.image.image = DataHandler.instance.user?.get_profile_picture()
        profileViewController.name.text = DataHandler.instance.user!.name
        
        theme = DataHandler.instance.user!.get_setting().get_chosen_theme()
        
        accountTableview.alwaysBounceVertical = false
        accountTableview.separatorStyle = UITableViewCellSeparatorStyle.none
        goalButton.tintColor = theme
        themePickerButton.tintColor = theme
        
        accountViewController = self
        createGoalAlert()
        createThemeAlert()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let invalidCharacters = CharacterSet(charactersIn: "0123456789").inverted
        return string.rangeOfCharacter(from: invalidCharacters) == nil
    }
    // MARK: Set New Goal
    func createGoalAlert(){
        
        let noOfGoals = DataHandler.instance.user!.get_no_of_goals()
        let goal = DataHandler.instance.user!.get_selected_goal(noOfGoals).get_goal()
        
        self.goalButton.setTitle(String(goal), for: .normal)
        
        goalAlert = UIAlertController(title: "Your Daily Goal", message: nil, preferredStyle: .alert)
        
        
        goalAlert.addTextField { (textField) in
            
            textField.text = String(goal)
            textField.keyboardType = .numberPad
            textField.delegate = self
        }
        
        goalAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak goalAlert] (_) in
            let text = goalAlert!.textFields![0].text
            
            
            let test:Int? = Int(text ?? "")
           
            if(DataHandler.instance.user?.get_step_counter().check_date() == true){
                DataHandler.instance.user?.get_selected_goal(noOfGoals).set_goal(test!)
            }
            else{
                DataHandler.instance.user!.add_goal(test!)
            }
            let newNoOfGoals = DataHandler.instance.user!.get_no_of_goals()
            let newgoal = DataHandler.instance.user!.get_selected_goal(newNoOfGoals).get_goal()
            self.goalButton.setTitle(String(newgoal), for: .normal)
        }))
        
        
    }
    // MARK ThemeChooser
    func createThemeAlert(){
        
        let theme_index = DataHandler.instance.user!.get_setting().get_theme_index()
        //let availiable_themes = DataHandler.instance.user?.get_setting().get_available_themes()
        let current_theme_name = DataHandler.instance.user?.get_setting().get_available_themes()[theme_index].get_theme_name()
  
        self.themePickerButton.setTitle(current_theme_name, for: .normal)
        
        themeAlert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        populateThemeAlert()
    }
    

    func populateThemeAlert(){
        
        if let user = DataHandler.instance.user
        {
            let noOfThemes = (user.get_setting().get_available_themes().count)
            
            for i in (0...noOfThemes-1){
                 let theme_name = DataHandler.instance.user?.get_setting().get_available_themes()[i].get_theme_name()
                let theme_color = DataHandler.instance.user?.get_setting().get_available_themes()[i].get_main_color()
                let row = UIAlertAction(title: theme_name, style: .default, handler: { [weak themeAlert] (_) in
               //nogot.view.tintColor
                    self.setNewColor(i)
                    
                })
                row.setValue(theme_color, forKey: "titleTextColor")
                
                themeAlert.addAction(row)
                

            }
        }
    }
    
    // MARK: Set new theme color
    func setNewColor(_ index: Int){
        
        DataHandler.instance.user?.get_setting().set_chosen_theme(index: index)
        theme = DataHandler.instance.user!.get_setting().get_chosen_theme()
        mainviewController.tabbar.backgroundColor = theme
        mainviewController.footer.backgroundColor = theme
        goalButton.tintColor = theme
        mainviewController.Liquid.set_Color(theme)
        mainviewController.setThemeColor(theme)
        themePickerButton.tintColor = theme
        
        let theme_index = DataHandler.instance.user!.get_setting().get_theme_index()
        let current_theme_name = DataHandler.instance.user?.get_setting().get_available_themes()[theme_index].get_theme_name()
        
        self.themePickerButton.setTitle(current_theme_name, for: .normal)
        
        if(highscoreViewController != nil) {
            highscoreViewController.setColor(theme)
        }
        
        
    }

}
