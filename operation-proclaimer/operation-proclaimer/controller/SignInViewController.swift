//
//  SignInViewController.swift
//  operation-proclaimer
//
//  Created by Vincent Harruis on 2018-12-06.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import Foundation
import UIKit
import GoogleSignIn

class SignInViewController: UIViewController, GIDSignInUIDelegate
{
    override func viewDidLoad() {
        
        GIDSignIn.sharedInstance().uiDelegate = self
    }
}
