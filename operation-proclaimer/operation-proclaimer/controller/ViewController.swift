//
//  ViewController.swift
//  operation-proclaimer
//
//  Created by Edvin Holm on 2018-11-02.
//  Copyright © 2018 proclaimergang. All rights reserved.
//

import UIKit
import UserNotifications

var mainviewController: ViewController!

class ViewController: UIViewController, UIApplicationDelegate {
    
    var pageController: MainPageViewController!
    
    @IBOutlet weak var Liquid: LiquidView!
    @IBOutlet weak var stepsButton: UIButton!
    @IBOutlet weak var historyButton: UIButton!
    @IBOutlet weak var highscoreButton: UIButton!
    @IBOutlet weak var accountButton: UIButton!
    
    @IBOutlet weak var tabbar: UIView!
    @IBOutlet weak var footer: UIView!
    var dot : UIView!
    

    
    var startActive = true
    var first = true
    var background = false
    var firstSecond = false
    var noGoals: Int!
    var timer: Timer!
    var test: Int = 0
    var currentX: CGFloat!

    // Sets new page and moves the dot
    @IBAction func stepsButtonTapped(_ sender: UIButton) {
        pageController.setPage(0);
        dotMove(page: 0)
        
        
        if(!startActive){
            
            get_steps_from_goal()
            drawWave()
            startActive = true
        }
        
    }
    
    // Sets new page and moves the dot
    @IBAction func historyButtonTapped(_ sender: UIButton) {
        pageController.setPage(1);
        
        dotMove(page: 1)
        
        if(startActive){
            Liquid.fillLevel = 0
            Liquid.animateHeight()
            Liquid.hideText()
            startActive = false
            firstSecond = false
        }
    }
    
    // Sets new page and moves the dot
    @IBAction func highscoreButtonTapped(_ sender: UIButton) {
        pageController.setPage(2);
        dotMove(page: 2)
        if(startActive){
            
            Liquid.fillLevel = 0
            Liquid.animateHeight()
            Liquid.hideText()
            startActive = false
            firstSecond = false
        }
        
    }
    // Sets new page and moves the dot
    @IBAction func accountButtonTapped(_ sender: UIButton) {
        pageController.setPage(3);
        dotMove(page: 3)
        if(startActive){
            Liquid.fillLevel = 0
            Liquid.animateHeight()
            Liquid.hideText()
            startActive = false
            firstSecond = false
        }
    }
    //Sets the icon for each view and prepares for segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EmbedSegue"
        {
            self.pageController = segue.destination as? MainPageViewController
            self.pageController.currentButton = stepsButton
        
            stepsButton.setImage(UIImage(named: "walking.png"), for: .normal)
            historyButton.setImage(UIImage(named: "clock.png"), for: .normal)
            highscoreButton.setImage(UIImage(named: "trophy.png"), for: .normal)
            accountButton.setImage(UIImage(named: "profil.png"), for: .normal)
            
            self.pageController.buttons.append(stepsButton)
            self.pageController.buttons.append(historyButton)
            self.pageController.buttons.append(highscoreButton)
            self.pageController.buttons.append(accountButton)
            
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        Liquid.themeColor = DataHandler.instance.user?.get_setting().get_chosen_theme()
        tabbar.backgroundColor = Liquid.themeColor
        footer.backgroundColor = Liquid.themeColor
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dot = UIView()
        dot.layer.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        dot.frame = CGRect(x: ((tabbar.frame.width/4)/2), y: tabbar.frame.height-10, width: 10, height: 10)
        self.currentX = ((tabbar.frame.width/4)/2)
        let radius: CGFloat = dot.bounds.size.width / 2.0
        
        dot.layer.cornerRadius = radius
        dot.clipsToBounds = true
        
        self.tabbar.addSubview(dot)
        
        dot.backgroundColor = UIColor.white
        
        
        get_steps_from_goal()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        mainviewController = self
        

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.Liquid.liquid != nil {
            self.Liquid.animateAll()
        }
        
    }
    
    
    @objc func applicationDidBecomeActive() {
        
        self.Liquid.animateAll()
        if(startActive){
            self.Liquid.showText()
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if(first){
            dotMove(page: 0)
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(waveTimer), userInfo: nil, repeats: true)
            get_steps_from_goal()
            self.Liquid.createAll()
            drawWave()
            first = false
        }
    }
    
    func updateWaveHeight(_ fill: CGFloat){
        self.Liquid.fillLevel = fill
        self.Liquid.animateHeight()
        self.Liquid.showText()
    }
    
    func get_steps_from_goal(){
        
        if let user = DataHandler.instance.user
        {
            let noOfGoals = user.get_no_of_goals()
            let goal = user.get_selected_goal(noOfGoals)
        
            Liquid.goal = goal.get_goal()
            Liquid.steps = goal.get_steps()
            Liquid.fillLevel = CGFloat(Liquid.steps) / CGFloat(Liquid.goal)
        }
        
    }
    
    func setThemeColor(_ color: UIColor){
    Liquid.themeColor = color
    }
    
    func drawWave(){
        
        Liquid.animateHeight()
        Liquid.showText()
    }
    
    func dotMove(page: Int){
        let cellSize = (tabbar.frame.width/4)
        var newValue : CGFloat = 0
        let dotMove = CABasicAnimation(keyPath: "position.x")
        dotMove.fromValue = self.currentX
        dotMove.duration = 0.5
        dotMove.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
        
        if(page == 0){
            newValue = (cellSize/2)
            dotMove.toValue = newValue
        }else if(page == 1){
            newValue = (cellSize/2) + (cellSize)
            dotMove.toValue = newValue
        }else if(page == 2){
            newValue = (cellSize/2) + (cellSize * 2)
            dotMove.toValue = newValue
        }else if(page == 3){
            newValue = (cellSize/2) + (cellSize * 3)
            dotMove.toValue = newValue
        }
        
        dot.layer.add(dotMove, forKey: "slideAnimation")
        dot.layer.position.x = newValue
        currentX = dot.layer.position.x
        
    }

    @objc func waveTimer(){
   
        if (startActive){
            if(!firstSecond){
                
               
                firstSecond = true
            } else {
                get_steps_from_goal()
                drawWave()
            }
            
        }
        
    }
    
    func showSignIn()
    {
        let controller: SignInViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "sign_in") as! SignInViewController
    
        self.present(controller, animated: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if DataHandler.instance.user == nil
        {
            showSignIn()
        }
    }
}

